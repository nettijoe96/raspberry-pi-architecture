"""
Contains the pi class that is used in the orchestrator and in the pis.
"""

import threading
import pickle
from constants import *
from files import files, paths
import utility
import os
from orchestrator.simulation import *
import copy

class Pi:
    '''
    The Pi class is used by the orchestrator and the simulating nodes/pis for different purposes.
    The orchestrator uses the Pi class to keep track of the pis IP address, installed simulations, state, etc.
    The pis use the pi class to keep track of peers, IP, installed simulations, crls, etc.
    '''

    def __init__(self, IP):
        # self.IP = IP
        # self.currState = None
        # self.simulationList = []    #full of Simulation objects for orchestrator and full of SimulationProcesses for pis
        # self.dockerSubnetList = {}  #maps simulations to dictionary of container names to subnets
        # self.installedSimulations = []
        # self.runningSimulations = []
        # self.nextUnusedSubnet = nextUnusedSubnet
        # self.keyServerIPList = []
        # self.crlLock = threading.Lock()
        # self.keyServerRequestLock = threading.Lock()
        # self.openC2Peers = []
        # self.crl = None
        # self.myCert = None
        self.state = PiState(IP)
        self.crlLock = threading.Lock()
        self.keyServerRequestLock = threading.Lock()

    def __repr__(self):
        return 'Pi @ {}'.format(self.state.IP)

    # def saveToFile(self, filename=None):
    #     q = Queue(maxsize=0)
    #
    #     # item = q.get()
    #     if filename == None:
    #         filename = files.piDataFile
    #     # self.crlLock.acquire()
    #     self.crlLock.acquire()
    #     try:
    #         with open(filename, 'ab') as f:
    #             pickle.dump(self, f)
    #     finally:
    #         self.crlLock.release()
    #     # q.task_done()

    def startSimulation(self, simName, type, path=None):
        '''Start a simulation previously installed on this Pi.

        Get the simulation object from this Pi's list of simulations and
        execute the run script associated with it.  If the simulation exists,
        return True, otherwise return False.

        :param simName: name of the simulation to begin
        :return: True if the simulation was started successfully, False otherwise
        '''
        # presetFilePath = utility.getSimulationPath(simName) + '/' + files.simulationPresetFile
        # image, is_image = image_name(presetFilePath)
        # print(self.state.SimulationList)
        for sim in self.state.SimulationList:
            if sim.simName == simName:
                print('Starting ' + simName)

                # if a custom path was not provided, use the default location
                if path is None:
                    path = utility.getSimulationPath(simName)
                    runScript = path + '/' + files.simulationRunScript
                    # print("Simulation Path:", runScript)
                    if type == commands.DOCKER:
                        image = IP.ORCHESTRATOR + ':5000/' + simName
                        is_image = utility.checkImageExists(image)
                        if is_image:
                            print("Running image:", image)
                            print("\n")
                            os.system('sh ' + runScript + ' ' + IP.ORCHESTRATOR + ' ' + simName)
                            print("\n")
                    else:
                        # print("Type is not docker\n")
                        os.system('sh ' + runScript)
                    # TODO: add PID tracking

                    self.state.runningSimulations.append(sim)
                    self.state.save_to_file()
                    return True
                else:
                    print('Simulation ' + simName + ' not found')
                    return False

    def stopSimulation(self, simName):
        '''Kill all processes associated with the simulation.

        :param simName: the name of the simulation to stop
        :return: True if the simulation was stopped successfully, False otherwise
        '''
        for sim in self.state.SimulationList:
            if sim.simName == simName:
                print('Stopping ' + simName)
                for pid in sim.pids:
                    os.system('kill -9 ' + pid)
                self.state.remove_sim(sim)
                return True
        else:
            print('Simulation ' + simName + ' not found')
            return False

    def initSimulation(self, simName):
        '''Initalize a new simulation by creating a directory and docker network.

        Creates a new simulation named simName, overwriting any existing simulation
        with the same name.
        :param simName: the name of the simulation to create
        '''
        print('Initializing ' + simName)

        # TODO: determine if simulation is a docker simulation
        # remove old docker network called simName, if one exists,
        # then create a fresh network

        os.system('docker network rm ' + simName)
        os.system('docker network create ' + simName)

        # TODO: find a better way to deal with conflicts
        # remove any existing simulation folder to resolve conflicts,
        # then create an empty folder for the new simulation
        simPath = utility.getSimulationPath(simName)
        if os.path.exists(simPath):
            os.system('rm -rf ' + simPath)
        os.makedirs(simPath)

        # configure the docker subnet
        # self.state.dockerSubnetList[simName] = self.getNextUnusedSubnet()   #TODO Automatic subnet allocation
        # self.incrementNextUnusedSubnet()

        # create a new simulation object and add it to the Pi's list of simulations
        simulation = SimulationProcess(simName)
        for sim in self.state.SimulationList:
            if sim.simName == simName:
                print("Duplicate simulation object:", simName)
                # response = input("Override Simulation Object? y or n: ")
                # if response == 'y' or response == 'yes':
                #     self.state.SimulationList.remove(sim)
                #     self.state.SimulationList.append(simulation)
                #     print("Simulation object:", simName, "overwritten")
                # elif response == 'n' or response == 'no':
                break
        else:
            self.state.SimulationList.append(simulation)
        # print("Simulation List:", self.state.SimulationList)
        self.state.save_to_file()

        # update the pi.data file

    def setCurrentState(self, currState):
        self.state = currState
        self.state.save_to_file()

    def setCRL(self, crl):
        self.crlLock.acquire()
        try:
            self.state.crl = crl
        finally:
            self.crlLock.release()

    def newInstallSimulation(self, sim):
        self.state.installedSimulations += [sim]
        self.state.dockerSubnetList[sim.simName] = self.getNextUnusedSubnet()
        self.state.save_to_file()

    def isRunning(self, simName):
        for sim in self.state.runningSimulations:
            if sim.simName == simName:
                return True
        self.state.save_to_file()
        return False

    def getSimulation(self, simName):
        for sim in self.state.SimulationList:
            if sim.simName == simName:
                return sim
        # self.state.save_to_file()
        return None

    def getNextUnusedSubnet(self):
        ########
        return self.state.nextUnusedSubnet


    def incrementNextUnusedSubnet(self): #TODO recycle subnets eventually. But this really won't matter for awhile
        sub = self.state.nextUnusedSubnet.split(".")
        if int(sub[2]) < 255:
            sub[2] = str(int(sub[2]) + 1)
        else:
            sub[1] = str(int(sub[1]) + 1)
            sub[2] = str(1)
        self.state.nextUnusedSubnet = '.'.join(sub)
        self.state.save_to_file()
        return self.state.nextUnusedSubnet

    def getSubnet(self, simName):
        return self.state.dockerSubnetList[simName]


class PiState:

    #current rank
    rank = 0   #This may be changed in the future to ranking package which is an object that can go to previous rankings instead of using network object
    peersIP = []  # current peers  #TODO info passed by orchestrator
    prevState = None
    IP = ""
    softwareList = []  # current programs being run on pi for specific simulation #TODO info passed by orchestrator
    recentDataBatch = None  # most recent batch of data
    timestamp = ""
    permissionState = ""  # this is useful if the application has different levels of permissions for nodes

    def __init__(self, IP, prevState=None, timestamp=None, nextUnusedSubnet="172.1.1.2"):
        # self.softwareList = softwareList
        self.IP = IP
        self.currState = None
        self.SimulationList = []
        self.dockerSubnetList = {}
        self.installedSimulations = []
        self.runningSimulations = []
        self.nextUnusedSubnet = nextUnusedSubnet
        self.keyServerIPList = []
        self.prevState = prevState
        self.timestamp = timestamp
        self.openC2Peers = []
        self.crl = None
        self.myCert = None

    def changeRank(self, rank):
        self.rank = rank

    def remove_sim(self, sim):
        self.runningSimulations.remove(sim)

    #peerLst is a list of peers to add to self.peers. We check if they are already first
    def addPeers(self, peerLst):
        for peer in peerLst:
            newPeer = True
            for p in self.peersIP:
                if peer.IP == p.IP:
                    newPeer = False
            if newPeer:
                self.peersIP += [peer]

    def setPermissionState(self, name):
        self.permissionState = name

    def save_to_file(self, filename=None):
        if filename is None:
            filename = files.piDataFile
        with open(filename, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)


COMMAND_SERVER_COUNT = 1
FILE_SERVER_COUNT = 1
SERVER_COUNT = 2


class SimulationProcess:
    '''
    Class to hold simulation information, including name, Docker IP, state, and
    the PIDs of any child processes (needed to exit cleanly).
    '''

    def __init__(self, simName, pids=None, running=False):
        self.simName = simName
        if pids == None:
            self.pids = []
        else:
            self.pids = pids
        self.running = running
        self.dockerIPSubnet = '172.30.1.2/24'
        self.metadataFilename = files.simulationMetadata
        self.dataContainerName = ''

    def setSubnet(self, prefix):
        self.dockerIPSubnet = prefix

    def addPIDs(self, pids):
        self.pids += pids

    def setState(self, isRunning):
        self.running = isRunning

    def setSimulationMetadataName(self, filename):
        self.metadataFilename = filename

    def setDataContainerName(self, filename):
        self.dataContainerName = filename



# May be used in the future
# class RankingPackage:
#     #current Ranking
#     #previous rankObj
#     #reason for change
#     pass
