#get a socket and run a server

#go through every possible execute option, and call the function that is in

import utility
import ssl # the sockets returned by the functions created in socketUtil are ssl sockets; not sure if this import is necessary
import socketUtil
from constants import commands, ports
import piNode.docker.appSpecificExecute as appSpecificExecute


def executeCommand(command):
    #there are two types of commands: terminal/os and non-terminal/non-os.
    # More simply, commands that can be run on a terminal and not parsed by our function

    response = ""
    #python may not recognize command because it may not be present. The commands that are not present are not executed anyways because of processAppSpecificExecute
    if command.type == commands.SCAN: #creates appropriate simulation directory
        response = appSpecificExecute.scan(command.command)
    elif command.type == commands.QUERY:
        response = appSpecificExecute.query(command.command)
    elif command.type == commands.REPORT:
        response = appSpecificExecute.report(command.command)
    elif command.type == commands.DENY:
        response = appSpecificExecute.deny(command.command)
    elif command.type == commands.SNAPSHOT:
        response = appSpecificExecute.snapshot(command.command)
    elif command.type == commands.DETONATE:
        response = appSpecificExecute.detonate(command.command)
    elif command.type == commands.RESTORE:
        response = appSpecificExecute.restore(command.command)
    elif command.type == commands.SAVE:
        response = appSpecificExecute.save(command.command)

    return response


def respond(conn, availableCommands):
    data = conn.recvfrom(ports.BUFFER_SIZE)
    string = bytes.decode(data)
    response = ""
    command = utility.ExecutionCommand(string)
    if command.type not in availableCommands:
        command.command = commands.COMMAND_NOT_AVAILABLE
    else:
        response = executeCommand(command)
    conn.sendall(str.encode(command.command))
    conn.sendall(str.encode(response))

def processAppSpecificExecute():
    availableCommands = []
    for line in open("appSpecificExecute.py", "r"):
        ele = line.split(" ")
        if ele[0] == "def":
            functionName = ele[1].split("(")[0]
            type = utility.getExecutionType(functionName)
            if utility.isAppSpecific(type):
                availableCommands += [type]
    return availableCommands

def main():
    availableCommands = processAppSpecificExecute()

    # create a socket to listen for data on the Local OpenC2 port
    openC2Socket, context = socketUtil.createServerSideSocket(port=ports.LOCAL_OPENC2_PORT, backlog=1)

    # openC2Socket waits to accept a connection and responds to received command
    socketUtil.acceptSocket(openC2Socket, context, respond, (availableCommands))


if __name__ == "__main__":
    main()
