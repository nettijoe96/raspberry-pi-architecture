"""
The Certificate Authority (CA) class.
The CA can sign CSRs and create and distribute CRLs.
"""
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
import datetime
import threading
import utility

class CA:

    def __init__(self, key, cert):
        """
        initializes a new ca object from a key and a cert that are already loaded from key and cert files
        """
        self.cakey = key
        self.cacert = cert
        self.crl = None
        self.crlBuilder = None
        self.crlLock = threading.Lock()
        self.crlChanges = 0

    def createCert(self, csr):
        '''
        creates and signs a cert from a csr

        :param csr: the certificate signing request to complete
        :return: new certificate, signed by this CA
        '''
        subject = csr.subject
        issuer = self.cacert.subject

        cert = x509.CertificateBuilder() \
            .subject_name(subject) \
            .issuer_name(issuer) \
            .public_key(csr.public_key()) \
            .serial_number(x509.random_serial_number()) \
            .not_valid_before(datetime.datetime.utcnow()) \
            .not_valid_after(datetime.datetime.utcnow() + datetime.timedelta(days=10)) \
            .sign(self.cakey, hashes.SHA256(), default_backend())

        return cert

    def renewCert(self, cert):
        '''
        Renew a certificate by extending its expiration date

        :param cert: certificate to renew
        :return: certificate with new expiration date
        '''
        pass

    def addToCRL(self, revokeObj):
        """
        Adds a cert to the crl but does not sign crl
        """
        self.crlBuilder.add_revoked_certificate(revokeObj)

    def finalizeCRL(self):
        """
        signs the CRL
        """
        crl = self.crlBuilder.sign(private_key=self.cakey, algorithm=hashes.SHA256(), backend=default_backend())
        return crl

    def genCRL(self):
        """
        Generates a new unsigned crl with no revocations in it
        """
        issuer = self.cacert.subject
        one_day = datetime.timedelta(1, 0, 0)
        builder = x509.CertificateRevocationListBuilder()
        builder = builder.issuer_name(issuer)
        builder = builder.last_update(datetime.datetime.today())
        builder = builder.next_update(datetime.datetime.today() + one_day)
        return builder

    def createNewCRL():
        self.crlBuilder = self.genCRL()

    def setCRL(self, crl):
        self.crlLock.acquire()
        try:
            self.crl = crl
        finally:
            self.crlLock.release()
