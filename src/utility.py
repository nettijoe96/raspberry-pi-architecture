from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import hashlib
import OpenSSL.crypto
import os           # for getRepoPath()
import platform     # for OS specific IP --> determine which OS we're on
import socket       # for OS specific IP --> Windows IP detection
import subprocess
import json
import struct
import socketUtil
import msgpack

from constants import commands, OPENC2, ports
from files import paths, files

def readIPList(filename, type='peer'):
    '''
    Create a list of IP addresses from a text file.

    Lines beginning with '#' are comments.  Lines containing IP addresses
    must specify the type of the host followed by its IP, in the form
    'type=127.0.0.1'.

    Type defaults to 'peer', so general IP lists should use
    the peer connection type.
    '''
    IPList = []
    with open(filename, 'r') as f:
        for line in f:
            line = line.strip().strip('\n')

            # skip empty lines and lines beginning with '#'
            if line == '' or line.startswith('#'):
                continue

            # add the IP address of the correct type
            elem = line.split('=')
            if elem[0] == type:
                IPList += [elem[1]]

    return IPList


def getIPList(piList):
    return [pi.IP for pi in piList]


"""
TODO: load a simulation preset file on a pi when it is going to load a simulation
"""
def readSimPresetFile():
    pass


def listToCSV(lst):
    '''
    Convert a list to a CSV string.

    This assumes no elements of lst (or their string representations) contain commas.
    :param lst: the list to convert
    :return: string representation of list as CSV
    '''
    return ','.join([str(e) for e in lst])


"""
a command class that contains the command and the type
"""
class ExecutionCommand:

    def __init__(self, command, type=None):
        self.command = command

        # if type was not specified, use the command string to infer it
        self.type = type
        if type is None:
            self.type = ExecutionCommand.getType(command)

        # trim the type from the front to get just the args
        self.args = self.command.lstrip(self.type).strip()

    def __repr__(self):
        return '<ExecutionCommand: type=' + self.type + ', args=[' + self.args + ']>'

    @staticmethod
    def getType(command):
        '''
        Get the type for the input command string.

        The type is the first word in the string, if it matches
        a known syntax. If the type is not recognized, TERMINAL is returned
        :param command: the string command to get the type of
        :return: the type of the command (default = TERMINAL)
        '''
        typeList = [commands.INIT,
                    commands.START,
                    commands.STOP,
                    commands.DELETE,
                    commands.ADDPEERGOSSIPUPGRADE,
                    commands.QUERY,
                    commands.DOCKER,
                    commands.REMOVE]

        cmdList = command.split(' ')
        if cmdList[0] in typeList:
            return cmdList[0]
        else:
            return commands.TERMINAL

    @staticmethod
    def isAppSpecific(command):
        '''Determines if command type is one of the App Specific types.'''
        typeList = [commands.QUERY,
                    commands.SCAN,
                    commands.REPORT,
                    commands.DENY,
                    commands.SNAPSHOT,
                    commands.DETONATE,
                    commands.RESTORE]

        return type in typeList


def checkImageExists(image):
    p = subprocess.run("docker image ls | awk '{print $1}'", shell=True, stdout=subprocess.PIPE)
    out = bytes.decode(p.stdout).split('\n')
    for item in out:
        if item == image:
            return True
    return False


def checkContainerExists(container):
    p = subprocess.run("docker container ls | awk '{print $2}'", shell=True, stdout=subprocess.PIPE)
    out = bytes.decode(p.stdout).split('\n')
    for item in out:
        if item == container:
            return True
    return False


def image_in_registry(image):
    # os.system("curl http://192.168.0.101:5000/v2/_catalog")
    try:
        repositories = subprocess.run("curl http://" + getIP() + ":5000/v2/_catalog | awk '{print}'", shell=True, stdout=subprocess.PIPE)
        out = bytes.decode(repositories.stdout).split(":")
        # print(out)
        images = out[1].strip("[").strip("]}\n").split(",")
        for item in images:
            item = item.strip('"')
            # print(item)
            if item == image:
                return True
    except IndexError:
        return False

    # print(images)

    return False


### Replaced by ExecutionCommand.getType() static method
def getExecutionType(command):
    typeList = [commands.INIT,
                commands.START,
                commands.STOP,
                commands.DELETE,
                commands.ADDPEERGOSSIPUPGRADE,
                commands.QUERY,
                commands.DOCKER]

    cmdList = command.split(" ")
    if cmdList[0] in typeList:
        return cmdList[0]
    else:
        print(commands.TERMINAL)
        return commands.TERMINAL

### Replaced by ExecutionCommand.isAppSpecific() static method
def isAppSpecific(type):
    typeList = [commands.QUERY,
                commands.SCAN,
                commands.REPORT,
                commands.DENY,
                commands.SNAPSHOT,
                commands.DETONATE,
                commands.RESTORE]

    return type in typeList



"""
get IP address from system
"""
def getIP():
    system = platform.system()

    if system == 'Linux':
        # This subprocess will return a string of the active interfaces, separated by newlines.  The first one should be the
        # primary one (with the highest preference).  We could just hardcode "wlan0" (or "eth0" if we switch to hardline), but
        # this is more flexible.
        p = subprocess.run("ip a | awk '/state UP/ {print $2}' | sed 's/.$//'", shell=True, check=True, stdout=subprocess.PIPE)
        # Stip any whitespace and split on newlines. The first entry is now the interface name
        iface = bytes.decode(p.stdout).strip().split('\n')[0]

        # This subprocess will return the line of "ip addr show" containing the IP address as the second item (following "inet").
        # We strip whitespace to get a clean list, then split on spaces to separate inet from the IP address and other info.
        p = subprocess.run("ip addr show " + iface + " | grep 'inet '", shell=True, check=True, stdout=subprocess.PIPE)
        ip = bytes.decode(p.stdout).strip().split(' ')[1]

        # Finally, trim off the subnet mask and return just the address
        return ip.split('/')[0]

        # TODO: this works but is not guarunteed.  The above should be more reliable but is harder
        # p = subprocess.run(["hostname", '-I'], check=True, stdout=subprocess.PIPE)
        # IPLstStr = p.stdout
        # return bytes.decode(IPLstStr).split(" ")[0]
    elif system == 'Darwin':
        # assume en0  TODO: find way to identify active interface
        # get IP address of en0 -> ipconfig getifaddr en0
        p = subprocess.run(['ipconfig', 'getifaddr', 'en0'], check=True, stdout=subprocess.PIPE)
        return bytes.decode(p.stdout).strip()
    elif system == 'Windows':
        # use the socket library to get the IP address based on the local hostname
        hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
        return ip
    else:
        raise Exception('System not recognized, IP address could not be found')


"""
load the private key from a bytes (it is already loaded from file)
"""
def loadPrivKeyCryptography(bytes, password=None):
    private_key = serialization.load_pem_private_key(bytes, password, backend=default_backend())
    return private_key

def loadPrivKeyOpenSSL(fileType, pubBytes, passphrase=None):
    pk = OpenSSL.crypto.load_privatekey(fileType, pubBytes, passphrase)
    return pk

def loadPubKeyOpenSSL(fileType, bytes):
    pk = OpenSSL.crypto.load_publickey(fileType, bytes)
    return pk

"""
load CRL from byte data that has already been loaded from a file
"""
def loadCRLFromBytes(data):
    crl = x509.load_pem_x509_crl(data, default_backend())
    return crl


"""
loading the CSR DN information from a preset file in order to make a CSR object.
"""
def loadCSRFile(filename):
    csrDict = {}
    with open(filename) as f:
        for line in f:
            if line.startswith('#'):
                continue
            elif line == '':
                continue
            ele = line.split('=')
            csrDict[ele[0]] = ele[1].strip('\n')
    return csrDict




"""
build a revoked certificate object
"""
def buildRevokedCert(serial, date, reasonFlag):
    builder = x509.RevokedCertificateBuilder()
    #set time/date as current time/date
    builder = builder.revocation_date(date)
    #set CRL reason extension
    reason = x509.CRLReason(reasonFlag)
    builder = builder.add_extension(reason, critical=True)
    builder = builder.serial_number(serial)
    revoked_certificate = builder.build(default_backend())
    return revoked_certificate


"""
look at end of file extension and try to find file
"""
def determineFileType(file):
    ext = file.split(".")
    if ext[-1] == "py":
        return commands.PYTHON
    elif ext[-1] == "sh":
        return commands.SCRIPT
    elif ext[-1] == "tar":
        return commands.TAR
    else:
        return commands.OTHER


"""
get a container name from the simulation metadata file which is .metadata
"""
def getContainerNames():
    names = []
    for line in open(files.simulationMetadata):
        ele = line.split("=")
        if ele[0] == commands.DOCKER_IMAGE:
            names += [ele[1]]
    return names



"""
uses constant file directories to get the path of the repository
"""
def getRepoPath():
    path = os.getcwd()
    length = path.find(paths.ROOT_DIR) + len(paths.ROOT_DIR)
    return path[:length]

def getSourcePath():
    return getRepoPath() + '/src'

def getCertPath(certFile=None):
    path = getRepoPath() + '/' + paths.CERTS_DIR
    if certFile is not None:
        path += '/' + certFile
    return path

def getKeyPath(keyFile=None):
    path = getRepoPath() + '/' + paths.KEYS_DIR
    if keyFile is not None:
        path += '/' + keyFile
    return path

def getPiCertPath():
    return getCertPath(files.PI_CERT_FILE)

def getPiKeyPath():
    return getKeyPath(files.PI_KEY_FILE)

def getCaCertPath():
    return getCertPath(files.CA_CERT_FILE)

def getCaKeyPath():
    return getKeyPath(files.CA_KEY_FILE)

def getSimulationPath(simName=None):
    simPath = getRepoPath() + '/' + paths.SIMULATION_DIR
    if simName is not None:
        simPath += '/' + simName
    return simPath

def getCRLPath():
    return getRepoPath() + '/' + paths.CRL_DIR + '/' + files.CRL_FILE

def getCSRPath():
    return getRepoPath() + '/' + paths.CSR_DIR + '/' + files.CSR_FILE

def getCertSetPath():
    return '/'.join([getSourcePath(), paths.KEY_SERVER_DIR, files.KEY_SERVER_CERT_SET])

def getKeyServerPeerList():
    return '/'.join([getSourcePath(), paths.KEY_SERVER_DIR, files.PEERLIST_FILE])

"""
searches simulation metadata to get the location of a simulation (even if the location is docker)
"""
def getProgramLocation(simName):
    #this finds where the peerList is.
    simDir = getSimulationPath(simName)
    location = None
    type = None
    with open(simDir + '/' + files.simulationMetadata) as f:
        for line in f:
            ele = line.split("=")
            if ele[0] == "bridgeLocation":
                location = ele[1]
                if location == "None":
                    location = None
                    type = None
                elif location == "docker":
                    location = ele[2]
                    type = "docker"
                elif location == "file":
                    location = ele[2]
                    type = "file"
                break

    return type, location #if location == None then there is no bridge! that means most openc2 commands are not possible


def hashFile(filename):
    '''Calculate the SHA1 hash of the input file.

    :param filename: path to file to be hashed
    :return: string of hexadecimal digits representing the hash
    '''
    BUF_SIZE = 1024     # read file in 1kB chunks
    sha1 = hashlib.sha1()

    with open(filename, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)

    return sha1.hexdigest()


# # # # # # # # # #
# Crypto Functions
# # # # # # # # # #

def buildRevokedCert(serial, date, reasonFlag):
    '''Build a revoked certificate object.'''
    builder = x509.RevokedCertificateBuilder()
    #set time/date as current time/date
    builder = builder.revocation_date(date)
    #set CRL reason extension
    reason = x509.CRLReason(reasonFlag)
    builder = builder.add_extension(reason, critical=True)
    builder = builder.serial_number(serial)
    revoked_certificate = builder.build(default_backend())
    return revoked_certificate

def loadPrivKey_Cryptography(bytes, password=None):
    '''Load the private key from a bytes (it is already loaded from file).'''
    private_key = serialization.load_pem_private_key(bytes, password, backend=default_backend())
    return private_key

def loadPrivKey_OpenSSL(fileType, pubBytes, passphrase=None):
    pk = OpenSSL.crypto.load_privatekey(fileType, pubBytes, passphrase)
    return pk

def loadPubKey_OpenSSL(fileType, bytes):
    pk = OpenSSL.crypto.load_publickey(fileType, bytes)
    return pk

def loadCert_Cryptography(data):
    cert = x509.load_pem_x509_certificate(data, default_backend())
    return cert

def loadCert_OpenSSL(type, data):
    cert = OpenSSL.crypto.load_certificate(type, data)
    return cert

def loadCSR_Cryptography(data):
    """
    load CSR object from cryptography library from bytes
    :param data: csr bytes
    :return: csr object
    """
    csr = x509.load_pem_x509_csr(data, default_backend())
    return csr

def getCAPublicKey(cacert):
    pass

def signData(data):
    pass

def genRSAKeyFile(filePath):
    """
    Generates a key file
    :param filePath: path
    :return: returns bytes of private key
    """
    key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
    pk = key.private_bytes(encoding=serialization.Encoding.PEM, \
                           format=serialization.PrivateFormat.TraditionalOpenSSL, \
                           encryption_algorithm=serialization.NoEncryption())
    with open(filePath, "wb") as f:
        f.write(pk)
    return pk


def check_serial(serial_type):
    serial_lst = []
    for member in dir(OPENC2):
        if not member.startswith('__'):
            serial = getattr(OPENC2, member)
            serial_lst.append(serial)
    if serial_type in serial_lst:
        return True
    else:
        return False


def check_valid(input_str):
    valid_commands = ["Locate",
                      "Contain",
                      "Deny",
                      "Allow",
                      "Query",
                      "Scan",
                      "Report",
                      "Redirect",
                      "Sync",
                      "help",
                      "h",
                      "q",
                      "quit"]
    valid_commands_lower = [command.lower() for command in valid_commands]
    return input_str in valid_commands or input_str in valid_commands_lower


def read(SSLSock, size):
    data = b''
    while len(data) < size:
        data_tmp = SSLSock.recv(size - len(data))
        data += data_tmp
        if data_tmp == ' ':
            raise RuntimeError("socket connection broken")
    return data


def _send(SSLSock, msg):
    sent = 0
    while sent < len(msg):
        sent += SSLSock.send(msg[sent:])


def send_obj(SSLSock, msg):
    if SSLSock:
        frmt = "=%ds" % len(msg)
        packed_msg = struct.pack(frmt, msg.encode())
        packed_hdr = struct.pack("=I", len(packed_msg))
        _send(SSLSock, packed_hdr)
        _send(SSLSock, packed_msg)


def read_obj(SSLSock):
    size = msg_len(SSLSock)
    serial_type = read(SSLSock, 4)
    serial_type = struct.unpack('=I', serial_type)[0]
    data = read(SSLSock, size)
    frmt = "=%ds" % size
    msg = struct.unpack(frmt, data)
    # print(msg)
    return msg, serial_type


def msg_len(SSLSock):
    d = read(SSLSock, 4)
    length = struct.unpack('=I', d)
    return length[0]


def send_json(SSLSock, obj):
    obj = json.dumps(obj.__dict__)
    send_obj(SSLSock, obj)


def read_json(SSLSock):
    msg = read_obj(SSLSock)
    return json.loads(bytes.decode(msg[0][0]))


def send_msgpack(SSLSock, obj):
    print("Sending msgpack")
    msg = msgpack.packb(obj.__dict__)
    send_obj(SSLSock, msg)


def read_msgpack(SSLSock):
    msg = read_obj(SSLSock)
    return msgpack.unpackb(bytes.decode(msg[0][0]))


def convert(data):
    if isinstance(data, bytes):  return data.decode('ascii')
    if isinstance(data, dict):   return dict(map(convert, data.items()))
    if isinstance(data, tuple):  return map(convert, data)
    if isinstance(data, list):   return list(map(convert, [item for item in data]))
    return data


class SerialMessage:
    def __init__(self, action, target, parameters):
        self.serial_type = ''
        self.action = action
        self.target = target
        self.parameters = parameters
        self.user_cmd = {}

    def set_msg(self):
        self.user_cmd = {"Action": self.action, "Target": self.target, "Parameters": {"Arguments": self.parameters}}

    def set_type(self, serial_type):
        self.serial_type = serial_type

    def send_msg(self, SSLSock):
        # sock = socketUtil.createClientSideSocket(IP)
        # msg = json.dumps(serial_type)
        # send_obj(SSLSock, msg)
        msg = ''
        s_type = 0
        if self.serial_type == OPENC2.JSON:
            msg = json.dumps(self.user_cmd)
            msg = msg.encode()
            s_type = OPENC2.SERIAL_LST.index(OPENC2.JSON)
        elif self.serial_type == OPENC2.MSG_PACK:
            msg = msgpack.packb(self.user_cmd)
            s_type = OPENC2.SERIAL_LST.index(OPENC2.MSG_PACK)
        if SSLSock:
            frmt = "=%ds" % len(msg)
            packed_type = struct.pack("=I", s_type)
            packed_msg = struct.pack(frmt, msg)
            packed_hdr = struct.pack("=I", len(packed_msg))
            _send(SSLSock, packed_hdr)
            _send(SSLSock, packed_type)
            _send(SSLSock, packed_msg)
