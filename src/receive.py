import argparse
import os
import ssl
import subprocess

from constants import commands, ports, OPENC2
import files
import keyClient
import piNode.execute as execute
import sendUpgrade as upgrade
import utility
import json
import socketUtil
import threading
import socket
import msgpack


def receive(SSLSock, myPi):
    '''
    receive an input, create interpretable command or load a file.
    :param SSLSock: secure socket layer created between pi to receive data
    :param myPi: the address of the pi receiving data
    :param port: port socket binds to
    :return: command to execute or None if error or file was processed
    '''
    assert isinstance(SSLSock, ssl.SSLSocket)

    # get the connection address and port from the socket
    addr = SSLSock.getsockname()  # returns ('0.0.0.0', 8080)
    port = addr[1]
    # serial_type = ''
    # global send_type
    # send_type = True
    # count = 0
    # check if command or file by recieving data, checking first character, and then calling the appropiate function (file or command)
    if port == ports.FILE_PORT:
        print("receive:")
        error = receiveFile(myPi, SSLSock)
        return None, error
    elif port == ports.COMMAND_PORT:
        print("receive: ")
        data = b''
        # packet = SSLSock.recv(ports.BUFFER_SIZE)
        # while packet is not None:
        #     data += packet
        #     packet = SSLSock.recv(ports.BUFFER_SIZE)
        while True:
            packet = SSLSock.recv(ports.BUFFER_SIZE)
            if packet == commands.DATA_END:
                break
            else:
                data += packet
        string = bytes.decode(data)
        print(string)
        command = utility.ExecutionCommand(string)
        command, error = execute.executeCommand(myPi, command)
        print("Executed command")
        # os.system("docker rm -f registry")
        return command, error
    elif port == ports.KEY_SERVER_PORT:
        keyClient.receiveCRL(SSLSock, myPi)
    elif port == ports.OPENC2_PORT:
        msg, serial_type = utility.read_obj(SSLSock)
        msg = msg[0]
        # print(msg)
        serial_type = OPENC2.SERIAL_LST[serial_type]
        print("Serial type:", serial_type)
        receiveCommand(SSLSock, serial_type, msg)


def readHeader(packet):
    '''

    :param packet:
    :return:
    '''
    header = ''
    data = b''
    # headerStr = bytes.decode(packet)
    print('Header Str:')
    print(bytes.decode(packet))

    try:
        index = packet.index(commands.DATA_END)
        header = bytes.decode(packet[0:index])
        data = packet[index + len(commands.DATA_END):]
    except Exception:
        raise ValueError('packet did not include DATA_END')

    return header, data


# TODO: receive() expects a return value, yet this returns None.
# Should something be generating a return value?
def receiveFile(myPi, SSLSock):
    '''
    Receives header and file. Processes file based on contents of header.
    If header has -p then the file is put at the specifc path after -p.
    If header has -a then file is forwarded in clientPeerList.
    TODO: This function should be split up into more than 1 function, namely process receiveFile and processHeader.
    :param myPi: the address of the pi receiving the simulation file
    :param SSLSock: secure socket layer for data transfer
    :return:
    '''
    if not isinstance(SSLSock, ssl.SSLSocket):
        raise TypeError('SSLSock is not an ssl.SSLSocket')

    parse = argparse.ArgumentParser()
    parse.add_argument('filename')                              # positional argument: name of the file
    parse.add_argument('-p', '--path')                          # path to install the file, if not simulation/simName/
    parse.add_argument('-t', '--type')                          # specify the file type
    parse.add_argument('-d', '--delete', action='store_true')   # trigger the file to be deleted
    parse.add_argument('-c', '--command', nargs='*')            # command to execute along with file transfer

    filename = ''       # name of file to receive (first command argument)
    fileData = b''      # data contents of file (following header)
    isHeader = True     # flag to read header instead of data
    header = ''
    args = None         # arguments parsed from header

    # loop as long as there is data to receive
    # and either process the header or accumulate the data
    while True:
        data = SSLSock.recv(ports.BUFFER_SIZE)
        if data == None or data == b'':
            break
        if not isHeader:
            fileData += data
        else:
            # read the packet to separate the header from the initial data (if present)
            header, fileData = readHeader(data)

            print(header)
            headerList = header.split(' ')
            args = parse.parse_args(headerList)

            isHeader = False

    processFile(myPi, header, fileData, args)


# TODO: why all the sudo calls?
def processFile(myPi, header, data, args):
    '''
    Receive the simulation file name and find the path to the file in the repository under the simulation folder.
    Write the file to this location with the directory name same as the simulation file name.
    :param myPi: pi receiving the file and processing it
    :param header:
    :param data: contents of the file being sent to the pi
    :param args: the filename sent as a command line argument
    :return: A new directory on the pi with a simulation file in the directory
    '''
    # TODO: verify that the below behaves as desired
    filename = args.filename    # simName/fileName
    filepath = utility.getSimulationPath() + '/' + filename

    path = filepath.split('/')[:-1]             # slice off the filename, but keep the directory
    while not os.path.exists('/'.join(path)):   # wait until the directory for this file exists
        pass
    with open(filepath, 'wb') as f:             # when the folder exists, write the file into it
        f.write(data)

    # -p, --path
    if args.path is not None:
        # if the destination path doesn't exist, make it
        if not os.path.exists(args.path):
            os.makedirs(args.path, exist_ok=True)
        # move the file into the destination directory
        os.system('mv ' + filepath + ' ' + args.path)
        filepath = args.path + '/' + filename

    # -c, --command
    if args.command is not None:
        cmdList = args.command
        cmdList[0] = cmdList[0].lstrip('[')
        cmdList[-1] = cmdList[-1].rstrip(']')
        print(cmdList)

        if cmdList[0] == commands.UPDATE:
            simName = cmdList[1]
            action = cmdList[2]  # TODO make a command object
            if action == commands.RUN:
                if args.type == commands.PYTHON:
                    # run python
                    subprocess.call(["python3", filepath])
                elif args.type == commands.SCRIPT:
                    # run script
                    subprocess.call(["sh", filepath])
            elif action == commands.SAVE:
                # do nothing, file already written
                pass
        else:
            execute.executeCommand(myPi, utility.ExecutionCommand(' '.join(cmdList)))

    # -d, --delete
    if args.delete:
        os.remove(filepath)

    # -t, --type
    # if args.type == 'tar':
    #    subprocess.call(['tar', 'xvzf', filepath])
    # elif args.type == commands.DOCKER_IMAGE:
    #    os.system('sudo mv' + filepath + ' docker_images')
    #    os.system('sudo docker load -i docker_images/' + filename)
    #    os.system('sudo rm -r ' + filename)


def process_openc2_json(SSLSock, msg):
    data = json.loads(msg)
    while True:
        try:
            # data = utility.read_json(SSLSock)
            response = ''
            print(json.dumps(data, indent=4))
            print('----------------')
            for key, value in data.items():
                if key == "Action":
                    response = utility.getIP() + ": " + "Executing " + key + ": " + value
            SSLSock.sendall(response.encode())
            break
        except BaseException as e:
            raise e


def process_openc2_msgpack(SSLSock, msg):
    data = msgpack.unpackb(msg)
    data = utility.convert(data)
    for key, value in data.items():
        if isinstance(value, dict):
            for keys, items in value.items():
                for item in items:
                    item = utility.convert(item)
    while True:
        try:
            # data = utility.read_msgpack(SSLSock)
            response = ''
            print(json.dumps(data, indent=4))
            print('----------------')
            for key, value in data.items():
                if key == "Action":
                    response = utility.getIP() + ": " + "Executing " + key + ": " + value
            SSLSock.sendall(response.encode())
            break
        except BaseException as e:
            raise e


def receiveCommand(SSLSock, serial_type, msg):
    # print("Received command")
    if serial_type == OPENC2.JSON:
        msg = bytes.decode(msg)
        process_openc2_json(SSLSock, msg)
    elif serial_type == OPENC2.MSG_PACK:
        process_openc2_msgpack(SSLSock, msg)
