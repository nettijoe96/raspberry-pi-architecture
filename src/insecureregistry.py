import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--IP")                                                 # Host IP
args = parser.parse_args()
text = '{\n    "insecure-registries":["' + args.IP + ':5000"]\n}'
try:
    with open('/etc/docker/daemon.json', 'w') as config:
        config.write(text)
except FileExistsError or FileNotFoundError:
    if text in open('/etc/docker/daemon.json').read():
        print('Insecure registry already included\n')
        pass
    else:
        with open('/etc/docker/daemon.json', 'a') as config:
            config.write(text)
            print("Appended new insecure registry\n")
