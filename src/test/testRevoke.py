from constants import IP, ports, commands
import utility
import socketUtil
import sendUpgrade
import cryptography.x509.extensions as extensions
import keyClient 

"""
!!! BASICALLY keyClient/testRevoke
"""
def revokeCert():
    reason = extensions.ReasonFlags.unspecified
    revBytes = keyClient.buildRequestData(commands.REVOKE_REQUEST, (utility.getRepoPath() + "/certs/certs/revokeCert.pem", reason) ) #MAKE SURE TO MAKE THE REVOKE CERT!!
    return revBytes
"""
!!! THIS IS BASICALLY A COPY OF keyClient/keyServerRequest
"""
def sendRevoke(revokeBytes):
    keyServerIP = IP.KEY_SERVER
    sock = socketUtil.createClientSideSocket(keyServerIP)
    #sending revocation

    sendUpgrade.send(sock, keyServerIP, ports.KEY_SERVER_PORT, commands.REVOKE_REQUEST, revokeBytes)

revBytes = revokeCert()
sendRevoke(revBytes)


