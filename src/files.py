# Constants for directory paths and file names. This prevents hardcoded values from breaking if file paths change.

# NOTE: use the `paths` and `files` classes for new uses to preserve scope detail.
# i.e. instead of "from files import *", use "from files import paths"

# These global constants remain for compatibility.
# They should be removed once all references to
# them are converted to use the classes below.
# ================================================
# analyzerFiles = "simAnalyzer.txt"
# SIMULATION_DIR = "simulation"
# PI_NETWORK_DIR = "raspberry-pi-architecture"
# simulationPresetFile = "preset.txt"  #preset file changes this
# simulationObjectFile = "simulation.obj"
# simulationRunScript = "run.sh"
# simulationMetadata = ".metadata"
# KEY_SERVER_PEERLIST = "keyServer/peerlist.txt"
# piDataFile = "pi.data"
# CSR_PRESET_FILE = "../csr.txt"
# CSR_FILE = "certs/csr/pi.csr"
# PI_KEY_FILE = "certs/keys/pikey.pem"
# CA_KEY_FILE = "certs/keys/cakey.pem"
# PI_CERT_FILE = "certs/certs/picert.pem"
# CA_CERT_FILE = "certs/certs/cacert.pem"
# CRL_FILE = "certs/crl/crl.pem"
# KEY_SERVER_DIR = "keyServer"
# KEY_SERVER_CERT_SET = "keyServer/certSet.data"

class paths:
    DOCKER_IMAGES = '/docker_images'
    TEMP_DIR = '/temp_dir'

    SIMULATION_DIR = 'simulation'
    ROOT_DIR = 'raspberry-pi-architecture'
    NODE_DIR = 'piNode'
    KEY_SERVER_DIR = 'keyServer'
    ORCHESTRATOR_DIR = 'orchestrator'

    CERTS_DIR = 'certs/certs'
    KEYS_DIR = 'certs/keys'
    CRL_DIR = 'certs/crl'
    CSR_DIR = 'certs/csr'

class files:
    piDataFile = 'pi.data'
    analyzerFiles = 'simAnalyzer.txt'

    simulationPresetFile = 'preset.txt'
    simulationObjectFile = 'simulation.obj'
    simulationRunScript = 'run.sh'
    simulationMetadata = '.metadata'

    PEERLIST_FILE = 'peerlist.txt'
    CERT_SET_FILE = 'certSet.data'

    KEY_SERVER_CERT_SET = 'certSet.data'
    CSR_PRESET_FILE = '../csr.txt'
    CSR_FILE = 'pi.csr'
    CA_CERT_FILE = 'cacert.pem'
    CA_KEY_FILE = 'cakey.pem'
    PI_CERT_FILE = 'picert.pem'
    PI_KEY_FILE = 'pikey.pem'
    CRL_FILE = 'crl.pem'
