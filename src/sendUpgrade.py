"""
This file sends a software upgrade to all connected peers in clientPeerList

Data format of any upgrade: the upgrade files are zipped up into a tarball. The metadata of this tarball is an "upgrade number" that uniquely signifies an upgrade. The upgrade number is used in commands to reference a specific version of the upgrade.
There are 2 ports for system upgrades: 100 and 101. 100 is the file data (tarball) and 101 are commands on what to do with the upgrades.
"""
import os
import socket
import ssl
from threading import Thread, Lock

from constants import commands, ports, OPENC2
from files import files, paths
import socketUtil
import utility
import json


# TODO: what are these?
class command:
    commandStr = ""

class file:
    header = ""
    fileData = ""


def sendAll(IPList, data, sendType, port):
    '''Send data to all devices in the IP list.

    Each IP creates a new thread for communication, which allows asynchronous
    parallel communication.  This way, if a device fails to connect or takes
    a long time, the rest of the program does not hang.  The function does not
    return until all the threads are finished.

    @param IPList   The list of IP addresses to send data to
    @param port     The port to connect to for each IP
    @param sendType The type of data to be send (i.e. File Command, Key Server Request, etc.)
    @param data     Data to be sent
    '''
    # sanity check
    print('----------------')
    print('Sending to IP(s):')
    for ip in IPList:
        print('   ', ip + ':' + str(port))
    print('Data:')
    print('   ', data)
    print()

    # create a thread for each IP, and send the data to each in parallel
    threadList = []
    for IP in IPList:
        sock = socketUtil.createClientSideSocket(IP)
        t = Thread(target=send, args=(sock, IP, port, sendType, data))
        t.start()
        threadList.append(t)

    # wait until all threads have finished
    # before passing control back to main program
    while not threadList == []:
        # get a list of threads that are still running
        threadList = [t for t in threadList if t.is_alive()]

    print('........... DONE')
    print('----------------')



"""
sends data to a specific IP address.
"""
def send(sock, IP, port, sendType, data):
    '''Use the provided socket to send data to the specified address.

    @param sock     SSLSocket to use for connection
    @param IP       IP address (must be string of the form '1.2.3.4')
    @param port     Integer port number to connect to
    @param sendType Type of data to be sent
    @param data     Data to be sent
    @returns Response from the Key Server, if a request was made, otherwise None
    '''

    requestTypes = [commands.KEY_SERVER_REQUEST,
                    commands.SIGN_REQUEST,
                    commands.RENEWAL_REQUEST,
                    commands.REVOKE_REQUEST]

    while True:
        try:
            sock.connect((IP, port))
            if sendType == commands.EXECUTION_COMMAND:
                sendCommand(sock, data)
            elif sendType == commands.FILE_COMMAND:
                sendFile(sock, data)
            elif sendType == commands.STRING_FILE_COMMAND:
                sendStringAsFile(sock, data)
            elif sendType in requestTypes:
                response = sendRecKeyServer(sock, sendType, data)
                return response
            # elif sendType == commands.SERIAL:
            #     data[0].send_type(sock, data[1])
                # response = sock.recv(ports.BUFFER_SIZE)
            elif sendType == commands.OPENC2:
                data.send_msg(sock)
                response = sock.recv(ports.BUFFER_SIZE)
                return response
            # elif sendType == OPENC2.MSG_PACK:
            #     utility.send_msgpack(sock, data)
            #     response = sock.recv(ports.BUFFER_SIZE)
            #     return response
            sock.shutdown(socket.SHUT_RD)
            break
        except OSError as e:
            raise e


def sendRecKeyServer(sock, sendType, data):
    '''Sends a request to the Key Server and waits for a response.

    @param sock     SSLSocket which has been connected to the Key Server's address
    @param sendType Type of request being made
    @param data     Data to send as request
    @returns The response from the keyserver
    '''
    sock.sendall(data)
    sock.sendall(commands.DATA_END)

    # Read the incoming data. If no incoming data
    # is available, wait until the server responds
    response = sock.recv(ports.BUFFER_SIZE)
    # while response == b'':
    #     response = sock.recv(ports.BUFFER_SIZE)

    # after the initial response is received, continue
    # reading data until there is none left
    # data = sock.recv(ports.BUFFER_SIZE)
    # while data:
    #     response += data
    #     data = sock.recv(ports.BUFFER_SIZE)

    # interpret and return the response based on the send type
    if sendType == commands.SIGN_REQUEST:
        # the response should be the signed certificate
        signedCert = bytes.decode(response)
        print(signedCert)
        return signedCert
    elif sendType == commands.RENEWAL_REQUEST:
        # the response should be the renewed certificate
        # TODO: is there any processing to do here or just return the response?
        pass
    elif sendType == commands.REVOKE_REQUEST:
        # the response should be a confirmation of revocation
        confirmation = bytes.decode(data)
        return confirmation
    else:
        return bytes.decode(data)


def sendCommand(sock, command):
    '''Sends a command string to a specific socket'''
    sock.sendall(str.encode(command))
    sock.sendall(commands.DATA_END)


def sendFile(sock, headerStr):
    '''Send a file using the specified socket.'''

    # The header string contains the absolute path to the file,
    # but we only want to tell the client the filename, so pull
    # the name out of the path and amend the header before sending
    # headerList = headerStr.split(' ')
    # path = headerList[0]
    # pathList = path.split('/')
    # header = headerStr[len(path):]      # clip out the original path, but keep the arguments
    # header = pathList[-1] + header

    sock.sendall(str.encode(headerStr))
    # sock.sendall(commands.DATA_END)

    # After sending the header, we can send the file itself
    # as a string of bytes
    headerList = headerStr.split(' ')
    path = '/'.join([utility.getRepoPath(), paths.SIMULATION_DIR, headerList[0]])
    with open(path, 'rb') as f:
        data = f.read(ports.BUFFER_SIZE)
        while data:
            sock.sendall(data) #data already in bytes because 'rb'
            data = f.read(ports.BUFFER_SIZE)


# TODO: is this supposed to do more? If it is just
# to send a string, another name might be better
def sendStringAsFile(sock, string):
    # header format:
    sock.sendall(str.encode(string)) #send file
    # sock.sendall(commands.DATA_END)


def forward(peer, header):
    '''
    Forward is called by untrusted nodes to simply relay the commands and files forward.
    :param peer:
    :param header:
    :return:
    '''
    # TODO: complete this function
    pass


def checkCommand(inputStr):
    '''
    checks a command from keyboard input and returns this command back to main function
    :param inputStr:
    :return:
    '''
    #TODO: check return type and case handling

    inputList = inputStr.split(' ')
    if inputList[0] != commands.EXECUTION_COMMAND:
        return False
    if inputList[1] == "-a":
        pass
    else:
        IPList = inputList[1].split('.')
        for IP in IPList:
            isValid = checkIP(IP)
            if not isValid:
                return False

    return True


"""
checks a file header
"""
def checkFileHeader(inputStr):
    # TODO: check the file header
    return True


def buildFileHeader(filename, timestamp, command=None, path=None, type=None, delete=False):
    '''Create a header string to send to the receiving node(s).

    @param filename  The name of the file to be sent
    @param timestamp
    @param command (optional)   command to run on/with the file
    @param path
    @param type
    @param delete
    @returns    A string containing the filename, file hash, command, etc. in
                standard CLI format (file -arg1 data -arg2 data ...)
    '''

    # TODO: define receiving nodes?

    # fHash = utility.hashFile(filename)

    header = filename
    # header += " -h " + fHash
    if command != None:
        header += " -c " + command
    if path != None:
        header += " -p " + path
    if type != None:
        header += " -t " + type
    # header += " -s " + timestamp
    if delete:
        header += " -d "

    header += bytes.decode(commands.DATA_END)
    # print('Header length (chars): ' + str(len(header)))
    # print(header)
    return header


"""
Removes some unnecessary items from input in order to create the actual command or header that will be sent to receiving nodes
"""
def remakeCommandOrHeader(inputList):
    if inputList[0] == commands.EXECUTION_COMMAND:
        sendStr = ''
        for elem in inputList[2:]:
           sendStr += elem + ' '
        sendStr = sendStr[0:-1]             # TODO: is this neccessary?
        return sendStr
    elif inputList[0] == commands.FILE_COMMAND:
        sendStr = ''
        filelocation = inputList[2]
        for i in range(len(inputList)):
            # if the command string contains the -t option, and the
            # type is a Docker Image, rename and move the file, then
            # fix the command string to reflect the change
            if inputList[i] == "-t":
                fType = inputList[i+1]
                if fType == commands.DOCKER_IMAGE:
                    # may have to add a pull command here
                    # 'filelocation' here is just the image name
                    imageName = filelocation + "_image.docker"
                    os.system("docker save -o " + imageName + " " + filelocation)
                    os.system("mv " + imageName + " docker_images")
                    inputList[2] = "docker_images/" + imageName
        # sendStr is the new header
        for elem in inputList[2:]:
            sendStr += elem + ' '
        sendStr = sendStr[0:-1]
        return sendStr

def checkIP(IP):
    '''Verify that 'IP' is a correctly formatted IP address.'''
    addr = IP.split(".")
    if len(addr) != 4:      # if there are not 4 groups, the IP is invalid
        return False
    for sub in addr:        # for each group, make sure it is a number 0-255
        i = int(sub)
        if i not in range(0,256):
            return False
    return True

def parseIPList(IPListStr):
    '''Create a list of IP addresses from a comma separated string.'''
    IPList = IPListStr.split(",")
    for IP in IPList:
        if not checkIP(IP):
            print('Invalid IP address:', IP)
            return []
    return IPList

#TODO: Make help message command and file variable to the command.py file
def helpMessage():
    print("There are 2 types of data: commands and files \n")
    print("Commands: Tell pis to do certain operations")
    print("Types of commands: Git commands, start, stop, and delete")
    print("Options: -a is all IP address")
    print("git command: c [IP list or -a] <git command> <project id>")
    print("start: c [IP list or -a] start <project id>")
    print("stop: c [IP list or -a] stop <project id>")
    print("delete: c [IP list or -a] delete <project id>\n")

    print("Files: upgrade/application files")
    print("f [IP list or -a] <file name> <-id project id (optional)> <-t type>")


def init():
    projectPath = utility.getRepoPath()
    if not os.path.exists(projectPath + "/docker_images"):
        os.makedirs(projectPath + "/docker_images")


def main():
    '''
    Sends a command that is issued through CLI
    Calls some helper functions to check input and create official command or file header
    This may be separated into two functions
    :return:
    '''

    init()
    while True:
        print("Enter a command or file to send system-wide")
        inputStr = input()
        #inputStr = "f -a busybox -t docker"

        #get IP List from file
        IPList = utility.readIPList(files.PEERLIST_FILE)

        if inputStr == "help" or inputStr == "h" or inputStr == "help " or inputStr == "h ":
            helpMessage()
        elif inputStr == "q" or inputStr == "quit":
            break
        else:
            inputList = inputStr.split(" ")
            #IPs to send to
            sendingIPList = []
            if inputList[1] == "-a":
                sendingIPList = IPList
            else:
                sendingIPList = parseIPList(inputList[1])
                if len(sendingIPList) == 0:
                    print("Not a valid IP list. Format should be: IP1,IP2,...,IPN")
                    raise IOError

            isValid = False
            #Commands to send to
            if inputList[0] == "c" or inputList[0] == "command":
                isValid = checkCommand(inputStr)
                if isValid:
                    sendAll(sendingIPList, remakeCommandOrHeader(inputList), commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

            elif inputList[0] == "f" or inputList[0] == "file":
                isValid = checkFileHeader(inputStr)
                if isValid:
                    sendAll(sendingIPList, remakeCommandOrHeader(inputList), commands.FILE_COMMAND, ports.FILE_PORT)


if __name__ == "__main__":
    main()
