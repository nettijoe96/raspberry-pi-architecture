import ssl
import socket
import utility

def createServerSideSocket(port, backlog=5):
    '''Create a socket and SSLContext for server-side use.

    The server is the device that opens a socket and waits for something
    else to connect.  This function creates a standard Python socket that
    is bound to the local address on the specified port.  The optional
    backlog parameter specifies how many pending connections to allow before
    rejecting new ones.

    NOTE: The socket created by this function is not encrypted, and must be
    wrapped with the returned SSLContext to provide secure communication.

    @param port                 Integer port number to bind to
    @param backlog (optional)   Number of pending connections to allow. Defaults to 5
    @returns A socket bound to `port`
    @returns SSLContext suitable for wrapping the socket
    '''
    context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    # TODO: password is passed automatically. Remove in the future
    context.load_cert_chain(certfile=utility.getPiCertPath(), keyfile=utility.getPiKeyPath(), password='password')

    sock = socket.socket()
    sock.bind(('', port))
    sock.listen(backlog)

    return sock, context

def createClientSideSocket(server_ip):
    '''Create an SSL-wrapped socket for client-side use.

    The client is the device that temporarily connects to the server to request or send data.  This function creates an SSLSocket that can be
    connected to the address of the desired server.

    @param server_ip The hostname (IP address) of the server to connect to
    @returns SSLSocket
    '''
    context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=utility.getCaCertPath())

    conn = context.wrap_socket(socket.socket(), server_hostname=server_ip)
    return conn

def acceptSocket(sock, context, func, args):
    '''Create a server side connection to an existing socket using an SSLContext.

    This function will spin until a client calls connect on the IP address of this
    device with the correct port.  Accepting the connection creates a new socket object
    which is then wrapped by the provided SSLContext. We then call the function `func`
    by passing the SSLSocket object followed by any other arguments required by the function.  When the function returns (or exits with error) the socket is closed.

    @param sock     Plain Python socket that is bound and listening on (IP, port)
    @param context  SSLContext specifying certificate and key files.
                    Purpose should be Purpose.CLIENT_AUTH
    @param func     Function to call in order to process incoming data
    @param args     Additional arguments to func
    '''
    # ensure that the provided parameters are the correct types
    assert isinstance(sock, socket.socket)
    assert isinstance(context, ssl.SSLContext)

    while True:
        # wait for an incoming connection, create a new socket, and wrap it
        # using the provided context
        newSocket, fromAddr = sock.accept()
        print("accepted conn")
        connStream = context.wrap_socket(newSocket, server_side=True)
        # call the provided function, closing the socket when the function exits
        try:
            func(connStream, *args)
        finally:
            try:
                connStream.shutdown(socket.SHUT_RDWR)
            except OSError as e:
                pass
            connStream.close()




# class SecureSocket:
#     '''Wrapper for SSL Socket class.
#
#     This is a thin wrapper around the SSL library's SSL socket
#     that provides several custom class functions, such as binding
#     and running a custom funtion on connect.
#     '''
#     socket = None               # ssl.SSLSocket - wrapped socket object
#     allowConnections = False    # flag for allowing new connections
#
#     # utility.getPiKeyPath().strip()
#     # utility.getPiCertPath().strip()
#     # utility.getCaCertPath().strip()
#     def __init__(self, keyfile=None, certfile=None, ca_certs=None, server_side=False, cert_reqs=ssl.CERT_REQUIRED, ssl_version=ssl.PROTOCOL_TLS, ciphers='DEFAULT'):
#         '''Initialize a new SecureSocket object (SSL socket).'''
#         s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
#         self.socket = ssl.wrap_socket(s, keyfile=keyfile, certfile=certfile, server_side=server_side, cert_reqs=cert_reqs, ssl_version=ssl_version, ciphers=ciphers, ca_certs=ca_certs)
#
#     def getSocket(self):
#         '''
#         Return the socket object.
#         Use this instead of accessing directly to maintain abstraction.
#         '''
#         return self.socket
#
#     def listen(self, ipaddr='', port=None, backlog=None):
#         '''
#         Bind the socket to a specific port and listen for incoming data.
#         '''
#         self.socket.bind((ipaddr, port))
#         self.socket.listen(backlog)
#
#     def allowConnections(self, shouldAllow=True):
#         self.allowConnections = shouldAllow
#
#     def accept(self, func, args):
#         '''
#         Wait for new incoming connections and run the specified function on the connection. This should be the target of a separate thread so the main
#         thread is not stuck waiting for connections.
#         '''
#
#         self.allowConnections = True
#         # Loop as long as this remains true.  If the main
#         # thread sets this to False, the loop will exit.
#         while self.allowConnections:
#             try:
#                 # conn: SSLSocket
#                 # addr: address of connection
#                 conn, addr = self.socket.accept()
#                 func(conn, *args)
#             except socket.timeout:
#                 pass
