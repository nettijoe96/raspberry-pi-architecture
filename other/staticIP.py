import dhcp

dhcpPreset = "documentation/templates/dhcpcd.conf"
START_DHCP = "#START_WIRELESS_STATIC_IP"
END_DHCP = "#END_WIRELESS_STATIC_IP"
mydhcp = "mydhcpcd.conf"

#write new /etc/dhcpcd.conf file
def main():
    IP = requestIPFromDHCP()
    writeNewDHCPFile(IP)

def requestIPFromDHCP():
    IP = dhcp.requestNewIP()
    return IP

def writeNewDHCPFile(IP):
    w = 0
    with open(dhcpPreset, 'r') as f:
        with open(mydhcp, 'w+') as p:
            for line in f:
                if line == START_DHCP:
                    p.write(line)
                    w = 1
                elif line == END_DHCP:
                    w = 0
                elif w == 1:
                    ele = line.split(" ")
                    word2 = ele[1].split("=")
                    if word2[0] == "ip_address":
                        ele[1] = word2[0] + "=" + IP + "\n"
                        p.write(ele[0][1:] + ele[1])
                    else:
                        p.write(line[1:]) #taking off # from line
                else:
                    p.write(line)

main()
