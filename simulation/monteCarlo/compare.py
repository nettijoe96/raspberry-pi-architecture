#Susan Ni
#7/23/18
#Comparing runtime between different approaches
#created new functions for plotting but still  using functions from utility.py and collectData.py

from random import random
import time as t
import math

import multiprocessing

import matplotlib.pyplot as plt
import numpy as np
import math

import utility
import collectData

#-----------------------------------

basic = ['r','g','b','c','m','y','k']
count = 0

'''------------------------------------
            DATA COLLECTION
------------------------------------'''

def simple(exp, trials):
    '''
    collects runtime data for calculating pi using 10^x sample points with variable number of trials using the simple approach
    :param exp: exponent for number of sample points (10^x)
    :param trials: max number of trials
    :return: list of number of trials, list of runtimes
    '''
    print('simple approach')
    start = t.time()

    times = []

    trialRng = utility.convertRange(1, trials, 1)

    for trial in trialRng:
        time = trialsData((exp, trial)) #can collect data here
        times.append(time)

    end = t.time()
    utility.printTime(end-start)

    return trialRng, times

def processes(exp, trials):
    '''
    collects runtime data for calculating pi using 10^x sample points with variable number of trials using multiprocessing with Processes
    :param exp: exponent for number of sample points (10^x)
    :param trials: max number of trials
    :return: list of number of trials, list of runtimes
    '''
    print('multiprocessing with Processes')
    start = t.time()

    m = multiprocessing.Manager()
    timeQ = m.Queue()

    trialRng = utility.convertRange(1, trials, 1)

    for trial in trialRng:
        p = multiprocessing.Process(target=trialsData, args=((exp,trial), timeQ))
        p.start()
    for a in range(len(trialRng)):
        p.join()

    data = [timeQ.get() for b in range(trials)]

    end = t.time()
    utility.printTime(end - start)

    return [datum[0] for datum in data], [datum[1] for datum in data]

def pools(exp, trials):
    '''
    collects runtime data for calculating pi using 10^x sample points with variable number of trials using multiprocessing with Pools
    :param exp: exponent for number of sample points (10^x)
    :param trials: max number of trials
    :return: list of number of trials, list of runtimes
    '''
    print('multiprocessing with Pools')
    start = t.time()

    trialRng = utility.convertRange(1, trials, 1)

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        times = pool.map(trialsData, [(exp, trial) for trial in trialRng])

    end = t.time()
    utility.printTime(end - start)

    return trialRng, times


def trialsData(params, queue=False):
    '''
    simulates data collection for a fixed number of trials and number of sample points to actually collect runtime data
    :param params: tuple that contains
        :param exp: exponent for 10^x sample points
        :param trials: number of trials
    :param queue: queue object; defaults to not using a queue
    :return: runtime of data collection in seconds
    '''
    exp, trials = params

    start = t.time()

    for a in range(trials):
        collectData.calculatePi(10 ** exp, timer=True)

    end = t.time()
    total = end - start

    if not queue:
        return total
    else:
        queue.put((trials, total))

'''-----------------------------------
            PLOTTING DATA
-----------------------------------'''

def plotTrials(rng, times, approach):
    '''
    plot trial data with line of best fit with log scale on y axis
    :param rng: range of x values to plot over (10^x exponent for number of sample points)
    :param times: array of times for y values
    :param approach: string for legend (processes, pools, threads, or simple)
    :return: no return
    '''

    for a in range(len(times)):
        plotData(rng, [math.log(time) for time in times[a]])
        utility.updateColor()

    # line of best fit with legend label
    x = [val for a in range(len(times)) for val in rng] #creating list of x values to correspond to each time
    y = [math.log(time) for subtimes in times for time in subtimes] #flattening times list

    plt.plot(rng, np.poly1d(np.polyfit(x, y, 1))(rng), basic[count] + '-', label=approach)
    updateBasic()

def plotTrials1DList(rng, times, approach):
    '''
    plot trial data with line of best fit for a 1D list of times
    :param rng: range of x values to plot over (10^x exponent for number of sample points)
    :param times: list of times for y values
    :param approach: string for legend (processes, pools, threads, or simple)
    :return: no return
    '''

    # times = [math.log(a) for a in times]

    plotData(rng, times)

    # line of best fit with legend label
    linFit = np.poly1d(np.polyfit(rng, times, 1)) #poly1d linear best fit object
    plt.plot(rng, linFit(rng), basic[count] + '-', label=approach+'\n'+'y = '+str(linFit).lstrip())
    updateBasic()

def plotData(x, y):
    '''
    plot results
    :param x: list of x values
    :param y: list of y values
    :return: figure object
    '''

    plt.plot(x, y, markerfacecolor=basic[count], markeredgecolor='None', marker='o', linestyle='None')

def createSampledPointsFigure():
    plt.figure()

    plt.xlabel('# of sampled points (10^x)')
    plt.ylabel('time (log(s))')

def createTrialsFigure():
    plt.figure()

    plt.xlabel('# of trials')
    plt.ylabel('time (s)')

'''--------------------------------------
            HELPER FUNCTIONS
--------------------------------------'''

def updateBasic():
    global count
    count += 1
    if count >= len(basic):
        count = 0

'''------------------------
            MAIN
------------------------'''

# lo = 2
# hi = 7
# trials = 20
#
# createSampledPointsFigure()
#
# rng, errors, times = collectData.collectTrialData(lo, hi, trials=trials)
# plotTrials(rng, times, 'simple approach')
#
# rng, errors, times = collectData.poolTrials(lo, hi, trials=trials)
# plotTrials(rng, times, 'multiprocessing with Pools')
#
# rng, errors, times = collectData.multiprocessingTrials(lo, hi, trials=trials)
# plotTrials(rng, times, 'multiprocessing with Processes')
#
# plt.suptitle('runtime vs. # of sample points (10^x) for different approaches'+' ('+str(trials)+' trials)')
# plt.legend(loc='lower right')
# plt.show()

exp = 4
trials = 50

createTrialsFigure()

trialRng, times = simple(exp, trials)
plotTrials1DList(trialRng, times, 'simple approach')
print('rng', trialRng)
print('times', times)

trialRng, times = pools(exp, trials)
plotTrials1DList(trialRng, times, 'multiprocessing with Pools')
print('rng', trialRng)
print('times', times)

trialRng, times = processes(exp, trials)
plotTrials1DList(trialRng, times, 'multiprocessing with Processes')
print('rng', trialRng)
print('times', times)

plt.suptitle('runtime vs. # of trials for different approaches'+' (10^'+str(exp)+' sampled points)')
plt.legend(loc='upper left')
plt.show()