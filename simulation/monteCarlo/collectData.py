#Susan Ni
#7/20/18
#Data collection

from random import random
import time as t
import math

import threading
import queue

import multiprocessing

import utility

#-----------------------------------

def calculatePi(points, timer=True):
    '''
    Uses monte carlo method to calculate pi
    :param points: number of points to sample
    :param timer: boolean indicating if the timer should be turned on or not
    :return: estimation of pi, error between estimated value and real value,
             total time for calculation if timer param is True
    '''

    if timer == True:
        start = t.time()

    circle = 0  # number of points inside the quarter circle of radius 1

    for a in range(int(points)):
        x = random()
        y = random()
        if (x ** 2 + y ** 2) ** 0.5 < 1.0:  # if the (x,y) point is in the quarter unit circle
            circle += 1

    area = circle / points  # 0.25*{area of the circle}(pi*1^2)/{area of the square} = pi/4
    pi = area * 4

    if timer:
        end = t.time()
        total = end - start
        # utility.printTime(total)

    error = abs(math.pi - pi)
    ##    print('error: '+str(error))
    ##    print('pi estimation: '+str(pi))

    return pi, error, total if timer else None

# def calculatePi(points, timer=True):
#     '''
#     Uses monte carlo method to calculate pi
#     :param points: number of points to sample
#     :param timer: boolean indicating if the timer should be turned on or not
#     :return: estimation of pi, error between estimated value and real value,
#              total time for calculation if timer param is True
#     '''
#
#     if timer == True:
#         start = t.time()
#
#     circle = 0  # number of points inside the quarter circle of radius 1
#
#     xIn = []
#     yIn = []
#     xOut = []
#     yOut = []
#
#     for a in range(int(points)):
#         x = random()
#         y = random()
#         if (x ** 2 + y ** 2) ** 0.5 < 1.0:  # if the (x,y) point is in the quarter unit circle
#             circle += 1
#             xIn.append(x)
#             yIn.append(y)
#         else:
#             xOut.append(x)
#             yOut.append(y)
#
#     area = circle / points  # 0.25*{area of the circle}(pi*1^2)/{area of the square} = pi/4
#     pi = area * 4
#
#     if timer:
#         end = t.time()
#         total = end - start
#         utility.printTime(total)
#
#     error = abs(math.pi - pi)
#     ##    print('error: '+str(error))
#     ##    print('pi estimation: '+str(pi))
#
#     print('pi:',pi)
#     print('error',error)
#     print('number of points in circle:',circle)
#     print('number of sampled points:',points)
#
#     import plotting
#     plotting.graphCircleColors(xIn, yIn, xOut, yOut)
#
#     return pi, error, total if timer else None
#
# calculatePi(10**6)

def calculatePiMPI(points):
    '''
    Uses monte carlo method to calculate pi to be used for MPI
    :param points: number of points to sample
    :return: number of points that fell in the circle
    '''
    circle = 0  # number of points inside the quarter circle of radius 1

    for a in range(int(points)):
        x = random()
        y = random()
        if (x ** 2 + y ** 2) ** 0.5 < 1.0:  # if the (x,y) point is in the quarter unit circle
            circle += 1

    return circle


def collectTrialData(lo, hi, step=1, trials=1):
    '''
    calculates pi across a range of 10^x sample points for multiple trials
    :param lo: min value for exponent (10^x) inclusive
    :param hi: max value for exponent (10^x) inclusive
    :param step: step size for the range; defaults to a step size of 1
    :param trials: number of times to calculatePi over the range; defaults to 1
    :return: list of the range, array of errors, array of times
    '''
    print('simple approach')

    start = t.time()

    errors = []
    times = []
    rng = utility.convertRange(lo, hi, step)

    for a in range(trials):
        trialErrors = []
        trialTimes = []

        for b in rng:
            pi, error, time = calculatePi(10 ** b, timer=True)
            trialErrors.append(error)
            trialTimes.append(time)

        errors.append(trialErrors)
        times.append(trialTimes)

        trialErrors = []
        trialTimes = []

    end = t.time()
    total = end - start
    utility.printTime(total)

    return rng, errors, times


def multiprocessingTrials(lo, hi, step=1, trials=1):
    '''
    calculates pi across a range of 10^x sample points for multiple trials using multiprocessing
    :param lo: min value for exponent (10^x) inclusive
    :param hi: max value for exponent (10^x) inclusive
    :param step: step size for the range; defaults to a step size of 1
    :param trials: number of times to calculatePi over the range; defaults to 1
    :return: list of the range, array of errors, array of times

    ***CONCERNS: if the processes finish out of order, the colors of the plots won't correspond to the same trial***
    '''
    print('multiprocessing with Processes')

    start = t.time()

    m = multiprocessing.Manager()
    errorQ = m.Queue()
    timeQ = m.Queue()

    rng = utility.convertRange(lo, hi, step)

    for a in range(trials):
        p = multiprocessing.Process(target=collectDataQueue, args=(rng, errorQ, timeQ))
        p.start()
    for b in range(trials):
        p.join()

    end = t.time()
    total = end - start
    utility.printTime(total)

    return rng, [errorQ.get() for a in range(trials)], [timeQ.get() for b in range(trials)]


def poolTrials(lo, hi, step=1, trials=1):
    '''
    calculates pi across a range of 10^x sample points for multiple trials using Pools from multiprocessing library
    :param lo: min value for exponent (10^x) inclusive
    :param hi: max value for exponent (10^x) inclusive
    :param step: step size for the range; defaults to a step size of 1
    :param trials: number of times to calculatePi over the range; defaults to 1
    :return: list of the range, array of errors, array of times
    '''
    print('multiprocessing with Pools')

    start = t.time()

    rng = utility.convertRange(lo, hi, step)

    # with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
    #     data = pool.map(collectDataQueuePool, [rng for a in range(trials)])

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        data = pool.map(collectDataPool, [rng for a in range(trials)])

    end = t.time()
    total = end - start
    utility.printTime(total)

    return rng, [data[a][0] for a in range(trials)], [data[b][1] for b in range(trials)]

def collectDataPool(rng):
    '''
    calculates pi across a range of 10^x sample points and adds the errors and times to queue objects
    :param rng: range of x values to plot over (10^x exponent for number of sample points)
    :return: array of errors, array of times
    '''
    errors = []
    times = []
    for a in rng:
        pi, error, time = calculatePi(10 ** a, timer=True)
        errors.append(error)
        times.append(time)
    return errors, times

def threadedTrials(lo, hi, step=1, trials=1):
    '''
    calculates pi across a range of 10^x sample points for multiple trials using threads
    :param lo: min value for exponent (10^x) inclusive
    :param hi: max value for exponent (10^x) inclusive
    :param step: step size for the range; defaults to a step size of 1
    :param trials: number of times to calculatePi over the range; defaults to 1
    :return: list of the range, array of errors, array of times
    '''
    print('multithreading with Threading')

    start = t.time()

    threads = {}
    errorQ = queue.Queue()
    timeQ = queue.Queue()
    rng = utility.convertRange(lo, hi, step)
    for a in range(trials):
        threads[a] = threading.Thread(target=collectDataQueue, args=(rng, errorQ, timeQ))
        threads[a].start()
        # threads[a].join()

    end = t.time()
    total = end - start
    # printTime(total)

    return rng, [errorQ.get() for a in range(trials)], [timeQ.get() for b in range(trials)]


def collectDataQueue(rng, errorQ, timeQ):
    '''
    calculates pi across a range of 10^x sample points and adds the errors and times to queue objects
    :param rng: range of x values to plot over (10^x exponent for number of sample points)
    :param errorQ: queue object for error data
    :param timeQ: queue object for time data
    :return: no return
    '''
    errors = []
    times = []
    for a in rng:
        pi, error, time = calculatePi(10 ** a, timer=True)
        errors.append(error)
        times.append(time)
    errorQ.put(errors)
    timeQ.put(times)