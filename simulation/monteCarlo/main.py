#Susan Ni
#7/20/18

from mpi4py import MPI
import math
import time as t

from collectData import *
#from plotting import *
import utility
import fileData

#-----------------------------------

# plotTrials(*collectTrialData(1, 6, step=1, trials=100),approach='Simple approach', avg=False, line=False)
# plotTrials(*poolTrials(1, 6, step=1, trials=100),approach='Multiprocessing with Pools, avg=False, line=False)
# plotTrials(*multiprocessingTrials(1, 6, step=1, trials=10),approach='Multiprocessing with Processes', avg=False, line=False)

# collectTrialData(1, 7, step=1, trials=100)
# poolTrials(1, 7, step=1, trials=50)
# multiprocessingTrials(1, 7, step=1, trials=50)

'''
*** COMMENTS ***
 - Put/Get from Queues with Processes might take longer using the return values from the Pool?
 - Implement line of best fit instead of averages? Or both?
'''

#-----------------------------------

start = t.time()

comm = MPI.COMM_WORLD
size = comm.Get_size() #number of cpus, workers, or both combined?
rank = comm.Get_rank() #0 is manager and workers numbers >= 1

points = 10**7 #total number of points to sample

data = calculatePiMPI(points//size) #number of points in the circle

if rank == 0: #if manager node
    circle = data
    for a in range(1, size):
        workerData = comm.Recv(source=a)
        circle += workerData[0]

    area = circle / (points//size*size)  # 0.25*{area of the circle}(pi*1^2)/{area of the square} = pi/4
    pi = area * 4

    end = t.time()
    total = end - start

    utility.printTime(total)
    print('pi:', pi)
    print('error', abs(math.pi - pi))

    fileData.write('data.txt', points, total)

else: #if not manager, send collected data to the manager
    comm.Send([data], dest=0)
