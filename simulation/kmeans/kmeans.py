import time, math
from mpi4py import MPI
import numpy as np
from numpy import genfromtxt

data = genfromtxt('data_set.csv', delimiter=',', dtype=float)
# print(data)
# print(len(data))
true_cluster = genfromtxt('true_clusters.txt', delimiter=',', dtype=int)
# print(true_cluster)

num_clusters = 9  # Number of centers in dataset


def initialize_centroids(points, k):
    """returns k centroids from the initial points by copying the set of points and shuffling"""
    centroids = points.copy()
    np.random.shuffle(centroids)
    return centroids[:k]


def create_clusters(points, centroids):
    """Returns an array containing the index to the nearest centroid for each point"""
    # clusters = np.zeros(len(num_clusters))
    # for i in range(len(num_clusters)):
        # value = euclidean_distance(centroids[i], points)
    distances = np.sqrt(((points - centroids[:, np.newaxis])**2).sum(axis=2))
    return np.argmin(distances, axis=0)


def move_centroids(points, closest, centroids):
    """Returns an array of updated centroids assigned from the points closest to them"""

    return np.array([points[closest == k].mean(axis=0) for k in range(centroids.shape[0])])


def euclidean_distance(a, b):
    """Returns the euclidean distance between 2 points using numpy"""
    sum = 0.0
    for i in range(len(a)):
        sum += np.linalg.norm(a - b)
    final = math.sqrt(sum)
    return final


def stop(centroids, new_centroids, cutoff, k):
    """Returns a boolean true if the new centroids are far enough away to continue iterations"""
    for i in range(k):
        difference = euclidean_distance(centroids[i], new_centroids[i])
        return difference > cutoff


def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    global cutoff, dimensions, num_clusters, data, initial, min_dist
    cutoff = 0.2

    start = MPI.Wtime()

    num_points = len(data)
    dimensions = len(data[0])
    initial = initialize_centroids(data, num_clusters)
    print("initial centroids")
    #print(initial)

    dist = []
    min_dist = np.zeros(num_points)
    for point in data:
        dist.append(euclidean_distance(initial[rank], point))
        print("distances")
        #print(dist)

    temp_dist = np.array(dist)
    comm.Reduce(temp_dist, min_dist, op=MPI.MIN)
    comm.Barrier()
    if rank == 0:
        min_dist = min_dist.tolist()
    recv_min_dist = comm.bcast(min_dist, root=0)
    comm.Barrier()
    cluster = []
    for i in range(len(recv_min_dist)):
        if recv_min_dist[i] == dist[i]:
            cluster.append(data[i])
            print("clusters")
            #print(cluster)

    center = []
    center_val = [0] * dimensions
    for i in cluster:
        for j in range(dimensions):
            center_val[j] += float(i[j])

    for j in range(dimensions):
        if (len(cluster) != 0):
            center_val[j] = center_val[j] / len(cluster)

    center = comm.gather(center_val, root=0)
    comm.Barrier()
    if rank == 0:
        if stop(initial, center, cutoff, num_clusters):
            print(center)
            end = MPI.Wtime()
            print("Execution time %s seconds" % (end - start))

    initial = comm.bcast(center, root=0)
    comm.Barrier()


if __name__ == "__main__":
    main()
