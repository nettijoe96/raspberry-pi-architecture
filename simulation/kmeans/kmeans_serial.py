from sklearn.datasets import make_blobs
import numpy as np
import matplotlib.pyplot as plt
import time
import math

# create blobs
centers = [[-15, 6], [15, -15], [-1, -10], [1, 6], [-10, 10], [10, -7], [-15, -5], [-10, 2], [14 ,13]]
data = make_blobs(n_samples=10000, n_features=2, centers=centers, cluster_std=2, center_box=(1, 10), random_state=50)
# create numpy array for data points
points = data[0]

def initialize_centroids(points, k):
    """returns k centroids from the initial points by copying the set of points and shuffling"""
    centroids = points.copy()
    np.random.shuffle(centroids)
    return centroids[:k]


def create_clusters(points, centroids):
    """Returns an array containing the index to the nearest centroid for each point"""
    distances = np.sqrt(((points - centroids[:, np.newaxis])**2).sum(axis=2))
    return np.argmin(distances, axis=0)


def move_centroids(points, closest, centroids):
    """Returns an array of updated centroids assigned from the points closest to them"""
    return np.array([points[closest == k].mean(axis=0) for k in range(centroids.shape[0])])


def euclidean_distance(a, b):
    """Returns the euclidean distance between 2 points using numpy"""
    sum = 0.0
    for i in range(len(a)):
        sum += np.linalg.norm(a - b)
    final = math.sqrt(sum)
    return final
    # return np.linalg.norm(a-b)


def stop(centroids, new_centroids, cutoff, k):
    """Returns a boolean true if the new centroids are far enough away to continue iterations"""
    for i in range(k):
        difference = euclidean_distance(centroids[i], new_centroids[i])
        return difference > cutoff


start_time = time.time() # initial time for calculating total execution time


def main():
    """Iterates through Llyod's algorithm until a cutoff distance is reached"""
    cutoff = .0001  # stopping point for algorithm
    k = len(centers)  # number of clusters
    centroids = initialize_centroids(points, k)
    initial_centroids = centroids

    new_centroids = initialize_centroids(points,k)

    center = np.array([])
    membership = np.array([])
    for i in range(5):

        while stop(centroids, new_centroids, cutoff, k):
            # Assign each data point to its nearest centroid
            centroids = new_centroids
            clusters = create_clusters(points, centroids)
            # Recalculate the centroids using the new clusters
            new_centroids = move_centroids(points, clusters, centroids)
            print(euclidean_distance(centroids[1], new_centroids[1]))

        center = np.append(new_centroids, i)
        membership = np.append(clusters, i)
    print(center)
    print(membership)
    print(centroids.shape[0])
    print(len(center))
    print(len(membership))


    # Plot of data set without membership and initial centroids
    plt.scatter(points[:, 0], points[:, 1], s=5)
    plt.scatter(initial_centroids[:, 0], initial_centroids[:, 1], c='black', s=20)
    plt.xlim(-20, 20)
    plt.ylim(-20, 20)
    plt.show()

    # Plot of initial clusters and initial centroids
    plt.subplot(121)
    plt.scatter(points[:, 0], points[:, 1], c=data[1], cmap='viridis', s=5)
    plt.scatter(initial_centroids[:, 0], initial_centroids[:, 1], c='black', s=20)
    plt.xlim(-20, 20)
    plt.ylim(-20, 20)

    # Plot of final clusters and final centroids
    plt.subplot(122)
    plt.scatter(points[:, 0], points[:, 1], c=clusters, cmap='viridis', s=5)
    plt.scatter(new_centroids[:, 0], new_centroids[:, 1], c='black', s=20)
    plt.xlim(-20, 20)
    plt.ylim(-20, 20)

    print("--- %s seconds ---" % (time.time() - start_time))
    plt.show()
    print(5)



if __name__ == '__main__':
    main()

